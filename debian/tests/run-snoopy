#!/bin/sh

set -e

# Enable snoopy
debconf-set-selections debian/tests/snoopy.debconf
dpkg-reconfigure snoopy

# Make sure snoopy got preloaded
echo "~~ Checking content of /etc/ld.so.preload:"
cat /etc/ld.so.preload

# Run a command to be recognized by snoopy
sh -c 'ls -l /sbin'

# Evaluate the system journal
echo "~~ Evaluating snoopy logs"
snoopy_logs="$(journalctl -e -t snoopy)"
echo "$snoopy_logs"
if [ "$snoopy_logs" = '-- No entries --' ]; then
	echo 'Snoopy does not show up in the journal' 1>&2
fi
echo "$snoopy_logs" | grep 'ls -l /sbin$' >/dev/null
if [ $? != 0 ]; then
	echo 'Snoopy did not log the expected command' 1>&2
fi

# Purge snoopy, /etc/ld.so.preload should get removed
echo '~~ Purging snoopy'
dpkg --purge snoopy
if [ -f /etc/ld.so.preload ]; then
	echo "~~ Content of /etc/ld.so.preload:"
	cat /etc/ld.so.preload
	echo ' After purging snoopy etc/ld.so.preload still exists' 1>&2
fi
