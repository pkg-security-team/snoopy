Source: snoopy
Section: admin
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Marcos Fouces <marcos@debian.org>
Build-Depends: debhelper-compat (= 13), procps, socat
Standards-Version: 4.7.0
Homepage: https://github.com/a2o/snoopy/
Vcs-Git: https://salsa.debian.org/pkg-security-team/snoopy.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/snoopy
Rules-Requires-Root: no

Package: snoopy
Architecture: linux-any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: execve() wrapper and logger
 snoopy is merely a shared library that is used as a wrapper
 to the execve() function provided by libc as to log every call
 to syslog (authpriv).  system administrators may find snoopy
 useful in tasks such as light/heavy system monitoring, tracking other
 administrator's actions as well as getting a good 'feel' of
 what's going on in the system (for example Apache running cgi
 scripts).
 .
 This type of monitoring can be bypassed by hostile users, and should
 not be considered a secure replacement for tools like auditd.
